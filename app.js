const http = require('http')
http
  .createServer((req, res) => {
    const fileContent = ''
    let url = req.url
    if (url === '/home') {
      res.writeHead(200, { 'Content-type': 'text/html' })
      res.write('<h1>HOME</h1>')
      res.end()
    } else if (url === '/infos') {
      res.writeHead(200, { 'Content-type': 'text/html' })
      res.write('<h1>INFOS</h1>')
      res.end()
    } else {
      res.writeHead(404, { 'Content-type': 'text/html' })
      res.write('<h1>NOT FOUND PAGE</h1>')
      res.end()
    }
  })
  .listen(8080)
